extends AudioStreamPlayer


export(Array, AudioStreamOGGVorbis) var tracks : Array = Array()
export var starting_track : int = -1
var last : int = starting_track

func _ready() -> void:
	assert(tracks.size() > 1)
	if starting_track != -1:
		assert(starting_track >= 0 and starting_track < tracks.size())
		stream = tracks[starting_track]
		play()
	else:
		play_random()
	#WishAudioBus.get_volume("Background"))


func play_random() -> void:
	var rnd : int = last
	while rnd == last:
		rnd = randi() % tracks.size()
	last = rnd
	stream = tracks[rnd]
	play()


func _on_Musicplayer_finished() -> void:
	play_random()
