extends Spatial


onready var ET : Tween = $ExitTween
onready var PT : Tween = $PlayTween
onready var Cam : HorrorCam = $HorrorCam
onready var Intro : AnimationPlayer = $Intro
onready var env : Environment = $WorldEnvironment.environment

func _ready() -> void:
	call_deferred("find_players_name")
	Intro.play("Intro")
	if Global.intro_finished:
		Intro.advance(30.0)

func _process(_delta: float) -> void:
	if Intro.is_playing() and Input.is_action_just_pressed("skip_intro"):
		Intro.advance(30.0)

func find_players_name() -> void:
	var playername : String = ""
	if OS.has_environment("USERNAME"):
		playername = OS.get_environment("USERNAME")
	elif OS.has_environment("USER"):
		playername = OS.get_environment("USER")
		
	if not playername and OS.get_name() == "HTML5":
		playername = "DOWNLOAD ME"
	
	if playername:
		var texts : PoolStringArray = Cam.text
		playername[0] = playername[0].to_upper()
		texts.append(playername + "!")
		Cam.text = texts

func _on_Exit_hover_started() -> void:
# warning-ignore:return_value_discarded
	ET.remove_all()
	Cam.active = true
# warning-ignore:return_value_discarded
	ET.interpolate_property(Cam, "freq", null, 0.1, 2.0, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	ET.interpolate_property(Cam, "noise", null, 0.2, 1.0, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	ET.start()

func _on_Exit_hover_ended() -> void:
# warning-ignore:return_value_discarded
	ET.remove_all()
	Cam.active = false
# warning-ignore:return_value_discarded
	ET.interpolate_property(Cam, "freq", null, 1.0, 1.0, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	ET.interpolate_property(Cam, "noise", null, 0.0, 2.0, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	ET.start()


func _on_Exit_pressed() -> void:
	get_tree().quit()


func _on_Credits_pressed() -> void:
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://menu/Credits.tscn")


func _on_Itch_pressed() -> void:
# warning-ignore:return_value_discarded
	OS.shell_open("https://whiteshampoo.itch.io/")


func _on_Settings_pressed() -> void:
	$Settings.activate()


func _on_Intro_animation_finished(_anim_name : String) -> void:
	Global.intro_finished = true
	$GridMap/Buttons/Play.focus = true


func _on_Play_pressed() -> void:
	if PT.is_active():
		return
	print("play")
	Global.save_position = Vector3.ZERO
	Global.save_progress = -1
	env.fog_enabled = true
# warning-ignore:return_value_discarded
	PT.interpolate_property(env, "fog_depth_begin", 20.0, 1.0, 1.0)
# warning-ignore:return_value_discarded
	PT.interpolate_property(env, "fog_depth_end", 30.0, 1.0, 1.0)
# warning-ignore:return_value_discarded
	PT.start()


func _on_PlayTween_tween_all_completed() -> void:
	print("change")
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://Level.tscn")



func _on_Exit_focused() -> void:
	_on_Exit_hover_started()


func _on_Exit_unfocused() -> void:
	_on_Exit_hover_ended()
