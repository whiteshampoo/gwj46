tool
extends Spatial

signal pressed
signal hover_started
signal hover_ended
signal focused
signal unfocused

export var text : String = "Text" setget set_text
func set_text(t : String) -> void:
	text = t
	if not is_instance_valid(L3):
		return
	L3.text = text
	if not is_instance_valid(L2):
		return
	L2.text = " %s " % text
	var extents : Vector3 = Vector3.ZERO
	var pixel : float = L3.pixel_size / 2.0
	extents.x = L2.rect_size.x * pixel * 11.0
	extents.y = L2.rect_size.y * pixel * 11.0
	extents.z = pixel
	$Area/CollisionShape.shape.extents = extents
	
export var color_hover : Color = Color.yellow
export var color_focus : Color = Color.red

export var _focus : bool = false setget _set_focus
func _set_focus(f : bool) -> void:
	_focus = f
	set_focus(_focus)
export var focus_prev : NodePath = ""
export var focus_next : NodePath = ""

export var _target : NodePath = ""
onready var target : Spatial = get_node_or_null(_target)

onready var T : Tween = $Tween
onready var L3 : Label3D = $Label3D
onready var L2 : Label = $Label
onready var color_original : Color = L3.modulate
onready var node_focus_prev : Spatial = get_node_or_null(focus_prev)
onready var node_focus_next : Spatial = get_node_or_null(focus_next)

var hover : bool = false setget set_hover
func set_hover(h : bool) -> void:
	hover = h
	update_color()
	


onready var focus : bool = _focus setget set_focus
func set_focus(f : bool) -> void:
	focus = f
	if focus:
		emit_signal("focused")
	else:
		emit_signal("unfocused")
	update_color()


onready var original_label_translation : Vector3 = L3.translation

func _ready() -> void:
	if is_instance_valid(target) and target in get_children():
		remove_child(target)
		L3.add_child(target)
		
	L3.text = text
	if focus:
		for node in get_tree().get_nodes_in_group("Focusable"):
			if not node.get("focus") or node == self:
				continue
			if node.focus:
				push_warning("More than 1 has focus")
			node.focus = false
	update_color()


func _input(event: InputEvent) -> void:
	if not event is InputEventKey:
		return
	if focus:
		if is_instance_valid(node_focus_prev) \
		and (Input.is_action_just_pressed("ui_up")
		or Input.is_action_just_pressed("ui_left")):
			node_focus_prev.set_deferred("focus", true)
			set_focus(false)
		if is_instance_valid(node_focus_next) \
		and (Input.is_action_just_pressed("ui_down")
		or Input.is_action_just_pressed("ui_right")):
			node_focus_next.set_deferred("focus", true)
			set_focus(false)
		if Input.is_action_just_pressed("ui_accept"):
			emit_signal("pressed")


func update_color() -> void:
	if not is_instance_valid(L3):
		return
	if hover and not focus:
		L3.modulate = color_hover
		return
	if focus and not hover:
		L3.modulate = color_focus
		return
	if hover and focus:
		L3.modulate = color_hover.linear_interpolate(color_focus, 0.5)
		return
	L3.modulate = color_original


func _on_Area_mouse_entered() -> void:
	emit_signal("hover_started")
	set_hover(true)
# warning-ignore:return_value_discarded
	T.remove_all()
# warning-ignore:return_value_discarded
	T.interpolate_property(L3, "translation", null, original_label_translation + L3.transform.basis.z * 0.1, 0.05)
# warning-ignore:return_value_discarded
	T.start()


func _on_Area_mouse_exited() -> void:
	emit_signal("hover_ended")
	set_hover(false)
# warning-ignore:return_value_discarded
	T.remove_all()
# warning-ignore:return_value_discarded
	T.interpolate_property(L3, "translation", null, original_label_translation, 0.05)
# warning-ignore:return_value_discarded
	T.start()

func _on_Area_input_event(_camera: Node, event: InputEvent, _position: Vector3, _normal: Vector3, _shape_idx: int) -> void:
	if not event is InputEventMouseButton:
		return
	if event.pressed and event.button_index == BUTTON_LEFT:
		get_tree().set_group_flags(SceneTree.GROUP_CALL_REALTIME, "Focusable", "focus", false)
		set_focus(true)
		emit_signal("pressed")
