extends ColorRect


export var sub : NodePath = ""
onready var Sub : ColorRect = get_node(sub)


func _ready() -> void:
	hide()


func _process(_delta: float) -> void:
	if Sub.visible:
		return
	if Input.is_action_just_pressed("ui_cancel"):
		if get_tree().paused:
			get_tree().paused = false
			hide()
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			get_tree().paused = true
			show()
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func _on_Continue_pressed() -> void:
	assert(not Sub.visible)
	get_tree().paused = false
	hide()
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _on_Settings_pressed() -> void:
	Sub.show()


func _on_Exit_pressed() -> void:
# warning-ignore:return_value_discarded
	get_tree().paused = false
	get_tree().change_scene("res://menu/Menu.tscn")
