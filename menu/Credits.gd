extends Control


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var append : String = ""
	var credits : Dictionary = Wish.Credits.credits
	for group in credits:
		append += "\n\n\n[b]%s[/b]" % group 
		var subs : Dictionary = credits[group]
		for sub in subs:
			append += "\n\n%s\n" % sub
			var entries : Array = subs[sub]
			for entry in entries:
				append += "\n[url=%s]%s[/url] by %s" % [entry["url"], entry["res"], entry["dev"]]
				if entry["license"]:
					if Wish.Credits.licenses[entry["license"]]:
						append += " ([url=%s]%s[/url])" % [Wish.Credits.licenses[entry["license"]], entry["license"]]
					else:
						append += " (%s)" % entry["license"]
	$Text.bbcode_text %= append


func _on_Text_meta_clicked(meta : String) -> void:
	if not meta.begins_with("https://"):
		return
# warning-ignore:return_value_discarded
	OS.shell_open(meta)


func _on_Back_pressed() -> void:
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://menu/Menu.tscn")
