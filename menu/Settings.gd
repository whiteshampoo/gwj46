extends ColorRect

onready var Sub : Control = $"HBoxContainer/VBoxContainer/SubMenu/"

func _ready() -> void:
	deactivate()


func activate() -> void:
	show()
	mouse_filter = Control.MOUSE_FILTER_STOP
	Sub.mouse_filter = mouse_filter


func deactivate() -> void:
	hide()
	mouse_filter = Control.MOUSE_FILTER_IGNORE
	Sub.mouse_filter = mouse_filter


func _on_SubMenu_back() -> void:
	deactivate()
