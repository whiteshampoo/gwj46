extends Node

var intro_finished : bool = false

var save_position : Vector3 = Vector3.ZERO
var save_progress : int = -1

var test_fps : PoolRealArray = PoolRealArray()
var lower_quality : bool = true setget set_lower_quality
func set_lower_quality(q : bool) -> void:
	#print("set lower quality to %s" % q)
	WishSettings.set_value("lower_quality", q)
	lower_quality = q
	var envs : Array = get_tree().get_nodes_in_group("ENV")
	print(envs)
	for env in envs:
		env.environment.ss_reflections_enabled = not lower_quality

func _ready() -> void:
	Wish.Credits.add_license("CC-BY 3.0", "https://creativecommons.org/licenses/by/3.0/")
	Wish.Credits.add_license("MIT", "")
	Wish.Credits.add("Graphics", "Textures", "Metal 001", "ambientCG", "https://ambientcg.com/view?id=Metal001", "CC0")
	Wish.Credits.add("Graphics", "Textures", "Rock 017", "ambientCG", "https://ambientcg.com/view?id=Rock017", "CC0")
	Wish.Credits.add("Graphics", "Textures", "Rock 032", "ambientCG", "https://ambientcg.com/view?id=Rock032", "CC0")
	Wish.Credits.add("Graphics", "Textures", "Abstract Wall 5", "Share Textures", "https://www.sharetextures.com/textures/abstract/abstract_wall_5/", "CC0")
	Wish.Credits.add("Graphics", "Textures", "Tiles 089", "ambientCG", "https://ambientcg.com/view?id=Tiles089", "CC0")
	Wish.Credits.add("Graphics", "Textures", "Wood 006", "ambientCG", "https://ambientcg.com/view?id=Wood006", "CC0")
	Wish.Credits.add("Graphics", "Textures", "Metal 007", "ambientCG", "https://ambientcg.com/view?id=Metal007", "CC0")
	Wish.Credits.add("Audio", "Sounds", "Slow Squish", "Socapex", "https://opengameart.org/content/punches-hits-swords-and-squishes", "CC-BY 3.0")
	Wish.Credits.add("Audio", "Background", "Unnerving Ambient Sounds", "DHSFX", "https://dhsfx.itch.io/unnerving-ambient-sounds", "")
	Wish.Credits.add("World", "3D-Models", "KayKit - Dungeon Pack", "Kay Lousberg", "https://kaylousberg.itch.io/kaykit-dungeon", "CC0")
	Wish.Credits.add("Software", "Engine", "Godot", "Juan Linietsky, Ariel Manzur", "https://godotengine.org/", "MIT")
	Wish.Credits.add("Software", "Engine", "GIT-Plugin", "Twarit Waikar", "https://github.com/godotengine/godot-git-plugin", "MIT")
	#Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	OS.min_window_size = OS.window_size
	#get_viewport().size *= 1.0
	WishSettings.init_value("no_enemies", false)
	WishSettings.init_value("fullscreen", false)
	WishSettings.init_value("lower_quality", false)
	OS.window_fullscreen = WishSettings.get_value("fullscreen")



func _physics_process(_delta: float) -> void:
	if test_fps.size() < 300:
		test_fps.append(Engine.get_frames_per_second())
	else:
		var sum : int = 0
		for fps in test_fps:
			sum += fps
		sum /= test_fps.size() 
		#print("FPS: %f" % sum)
		if sum <= 20.0:
			set_lower_quality(true)
		set_physics_process(false)
			


func _input(event: InputEvent) -> void:
	if not event is InputEventKey:
		return
	if Input.is_action_just_pressed("toggle_fullscreen"):
		OS.window_fullscreen = not OS.window_fullscreen
		WishSettings.set_value("fullscreen", OS.window_fullscreen)
		get_tree().set_group("FullScreenCheckBox", "pressed", OS.window_fullscreen)
