extends Spatial

onready var Waypoints : GridMap = $Waypoints
onready var nav : AStar = Waypoints.nav
#onready var Noise : ColorRect = $Effects/Noise
#onready var Death : ColorRect = $Effects/Death
#onready var DeathGameOver : Label = $Effects/Death/GameOver
#onready var DeathShader : ColorRect = $Effects/Death/Shader
#onready var DeathTimer : Timer = $Effects/Death/DeathTimer
onready var Win : AnimationPlayer = $Effects/Win/AnimationPlayer

func _ready() -> void:
	Global.intro_finished = true
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	get_tree().set_group("Orbs", "nav", nav)
#	get_tree().set_group("Player", "Noise", Noise)
	

func die() -> void:
	
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://Death.tscn")
#	Noise.material.set_shader_param("strength", 0.0)
#	Death.show()
#	DeathGameOver.hide()
#	DeathShader.hide()
#	DeathTimer.start()
	

#
#func _on_DeathTimer_timeout() -> void:
#	if not DeathShader.visible:
#		DeathGameOver.hide()
#		DeathShader.hide()
#		DeathTimer.start()
#		return
## warning-ignore:return_value_discarded
#	get_tree().change_scene("res://menu/Menu.tscn")


func _on_Checkpoint_last_target_reached() -> void:
	Win.play("Win")

func _won() -> void:
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://menu/Menu.tscn")
