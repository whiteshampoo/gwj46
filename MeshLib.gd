tool
extends Spatial


export(String, DIR) var path : String = ""
export(float, 0.1, 2.0, 0.05) var mesh_scale : float = 1.0
export var mesh_translation : Vector3 = Vector3.ZERO
export var safety : bool = false
export var work : bool = false setget set_work

export(Array, Resource) var surface_overide : Array = Array()


func set_work(__ : bool) -> void:
	if not path:
		path = filename.get_base_dir()
	if not safety:
		return
	safety = false
	property_list_changed_notify()
	
	for child in get_children():
		child.queue_free()
		remove_child(child)
	
	var dir : Directory = Directory.new()
	WishFileUtils.folder_open(dir, path)
	
	dir.list_dir_begin(true, true)
	var file_name : String = "\\"
	while file_name:
		file_name = dir.get_next()
		
		if not file_name.ends_with(".glb"):
			continue
			
		var node : Node = load("%s/%s" % [path, file_name]).instance()
		if not node.get_child_count() == 1:
			continue
			
		var child : MeshInstance = node.get_child(0)
		node.remove_child(child)
		node.queue_free()
		
		add_child(child)
		child.owner = self
		child.scale = Vector3(mesh_scale, mesh_scale, mesh_scale)
		child.translation = mesh_translation
		
		child.create_trimesh_collision()
		
		if not surface_overide:
			continue
		
		if not surface_overide.size() == child.mesh.get_surface_count():
			return
		
		for i in child.mesh.get_surface_count():
			child.mesh.set("surface_%d/material" % i, surface_overide[i])
		
	dir.list_dir_end()

func _ready() -> void:
	set_process(false)
