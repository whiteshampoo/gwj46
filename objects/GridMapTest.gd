extends GridMap

var o : PoolIntArray = [0, 10, 16, 22]

func _ready() -> void:
	for cell in get_used_cells_by_item(mesh_library.find_item_by_name("tileBrickB_small")):
		var item : int = get_cell_item(cell.x, cell.y, cell.z)
		set_cell_item(cell.x, cell.y, cell.z, item, o[randi() % o.size()])
