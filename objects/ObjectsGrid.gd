extends GridMap

const ORI : Dictionary = {
	0: 0.0,
	10: TAU / 2.0,
	16: TAU / 4.0,
	22: TAU * 0.75,
}

export var torch : PackedScene = null

func _ready() -> void:
	assert(torch)
	var id : int = mesh_library.find_item_by_name("torchWall")
	for cell in get_used_cells_by_item(id):
		var ori : int = get_cell_item_orientation(cell.x, cell.y, cell.z)
		set_cell_item(cell.x, cell.y, cell.z, INVALID_CELL_ITEM)
		cell = cell * cell_size + translation * 2.0
		var child : Spatial = torch.instance()
		child.translation = cell
		child.rotation.y = ORI.get(ori, 0.0)
		get_parent().call_deferred("add_child", child)
