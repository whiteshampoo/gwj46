tool
extends MeshInstance

export(float, 0.01, 10.0, 0.01) var size : float = 1.0 setget set_size
func set_size(s : float) -> void:
	size = s
	update()


export var texture : Texture = null setget set_texture
func set_texture(t : Texture) -> void:
	texture = t
	update()


func update() -> void:
	if not texture:
		return
	mesh.material.albedo_texture = texture
	mesh.size.x = texture.get_width() / 1000.0 * size
	mesh.size.y = texture.get_height() / 1000.0 * size
