tool
extends Spatial

export var flame_radius : float = 0.1
export(float, 0.1, 1.0, 0.05) var flame_speed_min : float = 0.1 setget set_flame_speed_min
export(float, 0.1, 2.0, 0.05) var flame_speed_max : float = 0.2 setget set_flame_speed_max
export var darken : float = 0.2
export var color_1 : Color = Color.white
export var color_2 : Color = Color.antiquewhite


onready var FlameLight : OmniLight = $OmniLight
onready var FlameTween : Tween = $Tween

onready var original_position : Vector3 = $FlamePosition.translation


var P : Player = null
var T : Checkpoint = null


func set_flame_speed_min(speed : float) -> void:
	flame_speed_min = clamp(speed, 0.1, 1.0)
	flame_speed_max = max(flame_speed_max, flame_speed_min)
	property_list_changed_notify()


func set_flame_speed_max(speed : float) -> void:
	flame_speed_max = clamp(speed, 0.1, 2.0)
	flame_speed_max = max(flame_speed_max, flame_speed_min)


func _ready() -> void:
	set_process(false)
	new_tween()
	if get_parent().name == "Objects":
		return
	var Ps : Array = get_tree().get_nodes_in_group("Player")
	var Ts : Array = get_tree().get_nodes_in_group("Target")
	if not Ps.size() == 1 or not Ts.size() == 1:
		prints(Ps, Ts)
		push_error("Too many Players or Targets")
		return
	P = Ps[0]
	T = Ts[0]
	$Updater.start(rand_range(1.0, 10.0))


func new_tween() -> void:
	FlameTween.remove_all() # warning-ignore:return_value_discarded
	var random_position = Vector3.ZERO
	random_position.x = rand_range(-1.0, 1.0)
	random_position.y = rand_range(-1.0, 1.0)
	random_position.z = rand_range(-1.0, 1.0)
	var duration : float = rand_range(flame_speed_min, flame_speed_max)
# warning-ignore:return_value_discarded
	FlameTween.interpolate_property(FlameLight, "translation", null,
			random_position.normalized() * flame_radius + original_position,
			duration, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	FlameTween.interpolate_property(FlameLight, "light_color", null,
			lerp(color_1, color_2, randf()).darkened(randf() * darken),
			duration, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	FlameTween.start() # warning-ignore:return_value_discarded


func _on_Tween_tween_all_completed() -> void:
	new_tween()


func _on_Updater_timeout() -> void:
	if not is_instance_valid(P) or not is_instance_valid(T):
		return
	var _a : Vector3 = to_local(P.translation)
	var a : Vector2 = Vector2(_a.x, _a.z)
	var _b : Vector3 = to_local(T.translation)
	var b : Vector2 = Vector2(_b.x, _b.z)
	#var c : Vector2 = Vector2(translation.x, translation.z)
	if a.dot(b) > 0.0:
		FlameLight.hide()
		$Particles.emitting = false
	else:
		FlameLight.show()
		$Particles.emitting = true
	$Updater.start(rand_range(1.0, 10.0))
