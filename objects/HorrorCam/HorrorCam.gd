extends Camera
class_name HorrorCam

export var text : PoolStringArray = [
	"Run!",
	"Hide!",
	"It will kill you!",
	"Fear!",
	"Panic!",
	"It hunts you!",
	"Run for your life!",
	"Fast!",
	"Quick!",
	"Darkness!",
	"Run away!"
]

var freq : float = 1.0
var active : bool = false
var time : float = 0.0
var noise : float = 0.0 setget set_noise
func set_noise(n : float) -> void:
	noise = clamp(n, 0.0, 1.0)
	$Effects/Noise.material.set_shader_param("strength", noise * 0.8)

func calc_distance(distance : float, space : float) -> float:
	return distance * tan(deg2rad(fov) / 2.0) * (space / get_viewport().size.y)


func calc_height(distance : float) -> float:
	return calc_distance(distance, get_viewport().size.y)


func calc_width(distance : float) -> float:
	return calc_distance(distance, get_viewport().size.x)

func _ready() -> void:
	set_noise(0.0)


func _process(delta: float) -> void:
	if not active:
		return
	time += delta
	#set_noise(sin(time) / 2.0 + 0.5)
	if time >= freq:
		time = 0.0
		var label : LabelTemplate = preload("res://objects/HorrorCam/LabelTemplate.tscn").instance()
		label.text = text[randi() % text.size()]
		label.translation.z = -rand_range(10.0, 50.0)
		label.translation.x = rand_range(-1.0, 1.0) * calc_width(label.translation.z)
		label.translation.y = rand_range(-1.0, 1.0) * calc_height(label.translation.z)
		add_child(label)
		


