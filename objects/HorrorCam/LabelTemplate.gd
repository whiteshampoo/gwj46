extends Label3D
class_name LabelTemplate

onready var Fade : Tween = $Fade
onready var Move : Tween = $Move

var distance : float = 1.0

func _ready() -> void:
	rotation.x = rand_range(-TAU, TAU)
	rotation.y = rand_range(-TAU, TAU)
	rotation.z = rand_range(-TAU, TAU)
	rotation /= Vector3.ONE * 16.0
	
	modulate.a = 0.0
# warning-ignore:return_value_discarded
	Fade.interpolate_property(self, "modulate:a", null, 1.0, 0.75, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	Fade.start()
	
	var moveto : Vector3 = Vector3.ZERO
	moveto.z = rand_range(-1, 1)
	moveto.y = rand_range(-1, 1)
	moveto.x = rand_range(-1, 1)
	moveto = moveto.normalized() * distance
# warning-ignore:return_value_discarded
	Move.interpolate_property(self, "translation", null, translation + moveto, 1.5, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	Move.start()

func _on_Fade_tween_all_completed() -> void:
# warning-ignore:return_value_discarded
	Fade.interpolate_property(self, "modulate:a", null, 0.0, 0.75, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	Fade.start()


func _on_Move_tween_all_completed() -> void:
	queue_free()



