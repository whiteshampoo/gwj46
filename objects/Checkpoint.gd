extends Area
class_name Checkpoint

signal target_reached(num)
signal last_target_reached

var player : Player = null

export(Array, NodePath) var targets : Array = Array()
export(Array, NodePath) var activators : Array = Array()
var progress : int = 0

onready var Anim : AnimationPlayer = $AnimationPlayer

func _ready() -> void:
	for a in activators:
		get_node(a).cell_scale = 0.0
	progress = int(max(0, Global.save_progress))
	assert(targets)
	assert(get_node(targets[progress]) is Position3D)
	assert(targets.size() == activators.size() + 1)
	for p in progress:
		get_node(activators[p]).cell_scale = 1.0
		for i in 2:
			var new_orb : Orb = preload("res://objects/Orb.tscn").instance()
			get_parent().get_parent().call_deferred("add_child", new_orb)
	translation = get_node(targets[progress]).translation
	print(translation)
	Anim.play("RESET")
	call_deferred("search_player")


func search_player() -> void:
	player = get_tree().get_nodes_in_group("Player")[0]
	
	
func _physics_process(_delta : float) -> void:
	if not is_instance_valid(player):
		return
	if player.to_global(player.Cam.translation) == Vector3.UP:
		look_at(player.to_global(player.Cam.translation), Vector3.LEFT)
	else:
		look_at(player.to_global(player.Cam.translation), Vector3.UP)


func _on_Checkpoint_body_entered(body : Node) -> void:
	if not body is Player:
		return
	$CollisionShape.disabled = true
	Anim.play("hide")


func _on_AnimationPlayer_animation_finished(anim_name : String) -> void:
	if not anim_name == "hide":
		return
	Anim.play("RESET")
	Global.save_position = translation
	Global.save_progress = progress
	emit_signal("target_reached", progress)
	if progress < activators.size():
		get_node(activators[progress]).cell_scale = 1.0
	progress += 1
	if progress == targets.size():
		emit_signal("last_target_reached")
		queue_free()
		return
	translation = get_node(targets[progress]).translation
	$CollisionShape.disabled = false
