extends KinematicBody
class_name Orb

onready var view : RayCast = $RayCast
onready var subview : Array = view.get_children()
onready var Aura1 : MeshInstance = $Aura1
onready var Aura2 : MeshInstance = $Aura2
onready var SeeYou : Timer = $SeeYou
onready var NoiseTween : Tween = $NoiseTween
onready var Noise : AudioStreamPlayer3D = $Noise
onready var TryAgain : Timer = $TryAgain

var nav : AStar = null

var player : Player = null
var player_visible : bool = false
var player_hunting : bool = false
var player_last_known_position : Vector3 = Vector3.ZERO

var path : PoolVector3Array = PoolVector3Array()
var last_position : Vector3 = Vector3.ZERO

var speed : float = 1.0
var strong : bool = false

func _ready() -> void:
	if WishSettings.get_value("no_enemies"):
		queue_free()
		return
		
	set_deferred("nav", get_parent().nav)
	translation = Vector3.ONE * 1000000.0
	call_deferred("search_new_point")
	$AnimationPlayer.play("show")

func search_new_point() -> void:
	$Detection/CollisionShape.disabled = false
	if not is_instance_valid(nav):
		call_deferred("search_new_point")
		return
	var players : Array = get_tree().get_nodes_in_group("Player")
	assert(players.size() == 1)
	var p : Player = players[0]
	
	var points : Array = nav.get_points()
	points.shuffle()
	for point in points:
		var coord : Vector3 = nav.get_point_position(point)
		if coord.distance_to(p.translation) < 12.0:
			continue
		var orbs_ok : bool = true
		for orb in get_tree().get_nodes_in_group("Orbs"):
			if orb == self:
				continue
			if coord.distance_to(orb.translation) < 12.0:
				orbs_ok = false
				break
		if not orbs_ok:
			continue
		translation = coord
		$AnimationPlayer.play("show")
# warning-ignore:return_value_discarded
		path = path_to_random(0.0)
		show()
		return
	translation = Vector3.ONE * 100000.0
	TryAgain.start()
	return

func _physics_process(delta : float) -> void:
	strong = false
	if not is_instance_valid(nav):
		return
	if is_instance_valid(player):
		var distance : float = translation.distance_to(player.translation)
		Aura1.scale = Vector3.ONE * min(distance, 1.0)
		Aura2.scale = Vector3.ONE * min(distance, 1.0)
	
	var was_visible : bool = player_visible
	look()
	if not was_visible and player_visible:
		if is_instance_valid(player) and not player_hunting:
			player.Cam.text = [
				"Hide!",
				"It will see you!",
				"Run away!",
				"Watch out!",
			]
		print("start")
		SeeYou.start()
	if not player_visible and not SeeYou.is_stopped():
		SeeYou.stop()
		print("stop")
		
	if not path:
		if player_hunting:
			path = path_to_player(delta)
		else:
			path = path_to_random(delta)

	if not path:
		if player_visible:
			path.resize(1)
			path[0] = player_last_known_position
		else:
			return
	
	if translation.distance_to(path[0]) <= speed * delta * 1.1:
		path.remove(0)
	
	if not path:
		return
	
	if player_visible:
		if translation.distance_to(player_last_known_position) <= translation.distance_to(path[0]):
			path.resize(1)
			path[0] = player_last_known_position

# warning-ignore:return_value_discarded
	last_position = translation
	move_and_slide(translation.direction_to(path[0]), Vector3.UP)
	#if is_equal_approx(last_position.x, translation.x) and is_equal_approx(last_position.y, translation.y):
		#path = path_to_random(delta) # stupid hotfix
		
	if translation.distance_to(player_last_known_position) <= 1.1:
		player_hunting = false

func look() -> void:
	player_visible = false
	if not is_instance_valid(player):
		return
	view.look_at(player.translation, Vector3.UP)
	view.force_raycast_update()
	if not view.is_colliding():
		return
	if not view.get_collider() is Player:
		return
	for ray in subview:
		if not ray is RayCast:
			continue
		ray.force_raycast_update()
		if not ray.is_colliding():
			return
		if not ray.get_collider() is Player:
			return
	#if not player_hunting:
		#print("gotcha")
		#path.resize(1)
	player_visible = true
	#player_hunting = true
	player_last_known_position = player.translation


func path_to_id(target_id : int, delta: float) -> PoolVector3Array:
	var my_id : int = nav.get_closest_point(translation)
	#var player_id : int = nav.get_closest_point(player_last_known_position)
	var _path : PoolVector3Array = nav.get_point_path(my_id, target_id)
	if _path and translation.distance_to(_path[0]) <= speed * delta * 1.1:
		_path.remove(0)
	if _path and not _path[0]:
		breakpoint
	return _path

func path_to_player(delta : float) -> PoolVector3Array:
	print("path_to_player")
	return path_to_id(nav.get_closest_point(player_last_known_position), delta)


func path_to_random(delta : float) -> PoolVector3Array:
	print("path_to_random")
	var ids : Array = nav.get_points()
	return path_to_id(ids[randi() % ids.size()], delta)

func _on_Detection_body_entered(body : Node) -> void:
	if body == self:
		return
	if body.is_in_group("Orbs"):
		if strong:
			return
		if not body.strong and bool(randi() % 2):
			strong = true
			return
		$AnimationPlayer.play("hide")
		$Detection/CollisionShape.disabled = true
		body.strong = true
		return
	if not body is Player:
		return
	assert(not player)
	player = body
	player.orbs.append(self)
	Noise.play()
# warning-ignore:return_value_discarded
	NoiseTween.remove_all()
# warning-ignore:return_value_discarded
	NoiseTween.interpolate_property(Noise, "unit_db", null, -15.0, 0.5)
	#print("Player entered")


func _on_Detection_body_exited(body : Node) -> void:
	if not body is Player:
		return
	assert(is_instance_valid(player))
	player.orbs.erase(self)
	player = null
# warning-ignore:return_value_discarded
	NoiseTween.remove_all()
# warning-ignore:return_value_discarded
	NoiseTween.interpolate_property(Noise, "unit_db", null, -100.0, 0.5)
	#print("Player exited")


func _on_SeeYou_timeout() -> void:
	print("found")
	player_hunting = true
	if is_instance_valid(player):
		player.Cam.text = [
			"Run!",
			"Hide!",
			"It will kill you!",
			"Fear!",
			"Panic!",
			"It hunts you!",
			"Run for your life!",
			"Fast!",
			"Quick!",
			"Darkness!",
			"Run away!"
		]


func _on_NoiseTween_tween_all_completed() -> void:
	if not is_instance_valid(player):
		Noise.stop()


func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "hide":
		search_new_point()


func _on_TryAgain_timeout() -> void:
	call_deferred("search_new_point")
