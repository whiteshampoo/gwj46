extends KinematicBody
class_name Player

const GRAVITY : float = 9.81
const MAX_SPEED : float = 1.0

onready var Cam : Camera = $HorrorCam

var orbs : Array = Array()
var nearest : float = -1.0
var Noise : ColorRect = null
var velocity : Vector3 = Vector3.ZERO

func _ready() -> void:
	if Global.save_progress >= 0:
		translation = Global.save_position

func _physics_process(delta: float) -> void:
	var joy : Vector2 = Input.get_vector("look_left", "look_right", "look_up", "look_down")
	if joy:
		move_cam(joy / 25.0)
	call_deferred("test_distance")
		
	var input : Vector2 = Input.get_vector("left", "right", "forwards", "backwards")
	
	var y_speed : float = velocity.y - GRAVITY * delta
	velocity = lerp(velocity, Vector3.ZERO, delta * 10.0)
	# * 1.0 if is_on_floor() else 1.0
	velocity.y = 0
	velocity = velocity.limit_length(MAX_SPEED)
	velocity.y = clamp(y_speed, -MAX_SPEED * 2.0, MAX_SPEED * 2.0)
	
	velocity += global_transform.basis.x * input.x
	velocity += global_transform.basis.z * input.y
	
	velocity = move_and_slide(velocity, Vector3.UP)


func test_distance() -> void:
	if not orbs:
		return
	nearest = 999999.999
	Cam.active = false
	for orb in orbs:
		var distance : float = translation.distance_to(orb.translation)
		if nearest > distance:
			nearest = distance

	var strength : float = 1.0 - clamp((nearest - 1.0) / 4.0, 0.0, 1.0)
	if strength > 0.0:
		Cam.active = true
	Cam.noise = pow(strength, 0.5)
	Cam.freq = 1.0 - strength
#
	#Noise.material.set_shader_param("strength", strength)
	if strength >= 0.99:
#warning-ignore:return_value_discarded
		get_tree().change_scene("res://Death.tscn")


func _input(event : InputEvent) -> void:
	if event is InputEventMouseMotion:
		#_input_mouse_motion(event)
		move_cam(event.relative / 250.0)



#func _input_mouse_motion(event : InputEventMouseMotion) -> void:


func move_cam(relative : Vector2) -> void:
	rotation.y -= relative.x
	Cam.rotation.x -= relative.y
	Cam.rotation.x = clamp(Cam.rotation.x, -TAU / 4.0, TAU / 4.0)

