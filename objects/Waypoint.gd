extends Area
class_name Waypoint

signal new_connection(my_id, other_id)

var nav_id : int = -1

var neighbours : Array = Array()

onready var Ray : RayCast = $RayCast
onready var Core : Area = $Core

func _ready() -> void:
	$RayCast/Line.hide()

func _on_Area_area_entered(area : Area) -> void:
	if not area.is_in_group("Waypoints"):
		return
	if area in neighbours:
		return
	neighbours.append(area)
	area.neighbours.append(self)
	Ray.translation = Vector3.ZERO
	Ray.look_at(area.translation, Vector3.UP)
	
	for x in range(-1, 2):
		for y in range(-1, 2):
			for z in range(-1, 2):
				if not x and not y and not z:
					continue
				Ray.translation = Vector3(x, y, z).normalized() * 0.24
				Ray.force_raycast_update()
				if not Ray.is_colliding():
					return
				if not Ray.get_collider().is_in_group("Waycores"):
					#print(Ray.get_collider())
					return
				if not Ray.get_collider() == area.Core:
					return
	var con : Spatial = $RayCast/Line.duplicate()
	add_child(con)
	con.translation = Vector3.ZERO
	con.look_at(area.translation, Vector3.UP)
	con.show()
	emit_signal("new_connection", nav_id, area.nav_id)
