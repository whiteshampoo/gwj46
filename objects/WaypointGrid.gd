extends GridMap

var nav : AStar = AStar.new()

var updates : bool = true

func _ready() -> void:
	hide()
	for cell in get_used_cells():
		cell *= cell_size
		cell = to_global(cell)
		cell += cell_size * Vector3(1.0, 0.0, 1.0) / 2.0
		cell.y += 0.125
		var waypoint : Waypoint = preload("res://objects/Waypoint.tscn").instance()
		waypoint.set_as_toplevel(true)
		waypoint.translation = cell
		waypoint.nav_id = nav.get_available_point_id()
		
		waypoint.connect("new_connection", self, "_new_connection") # warning-ignore:return_value_discarded
		add_child(waypoint)
		waypoint.visible = false
		nav.add_point(waypoint.nav_id, cell)

func _physics_process(_delta: float) -> void:
	if not updates:
		queue_free()
	updates = false


func _new_connection(my_id : int, next_id : int) -> void:
	nav.connect_points(my_id, next_id)
	updates = true
