extends ColorRect


func _ready() -> void:
	$AnimationPlayer.play("death")


func finished() -> void:
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://Level.tscn")
